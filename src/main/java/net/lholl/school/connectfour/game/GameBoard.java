package net.lholl.school.connectfour.game;

public class GameBoard {

	private final int[][] board;

	public GameBoard() {
		this.board = new int[6][7];
	}

	public GameBoard(int[][] board) {
		this.board = board;
	}

	public boolean validateSelection(int columnIndex) {
		return board[0][columnIndex] == 0;
	}

	public int checkForWin() {
		for (int player = 1; player < 3; player++) {
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 7; j++)
					if (board[i][j] == player && board[i + 1][j] == player && board[i + 2][j] == player && board[i + 3][j] == player) return player;
			for (int i = 0; i < 6; i++)
				for (int j = 0; j < 4; j++)
					if (board[i][j] == player && board[i][j + 1] == player && board[i][j + 2] == player && board[i][j + 3] == player) return player;
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 4; j++)
					if (board[i][j] == player && board[i + 1][j + 1] == player && board[i + 2][j + 2] == player && board[i + 3][j + 3] == player) return player;
			for (int i = 3; i < 6; i++)
				for (int j = 0; j < 4; j++)
					if (board[i][j] == player && board[i-1][j+1] == player && board[i-2][j+2] == player && board[i-3][j+3] == player) return player;
		}
		for (int i = 0; i < 7; i++) {
			if (board[0][i] == 0){
				return -1;
			}
		}
		return 0;
	}

	public int getBoardValue(int row, int column) {
		return board[row][column];
	}

	public void setBoardValue(int row, int column, int value) {
		board[row][column] = value;
	}

	public int[][] board() {
		var copy = new int[6][7];
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				copy[i][j] = board[i][j];
			}
		}
		return copy;
	}
}
