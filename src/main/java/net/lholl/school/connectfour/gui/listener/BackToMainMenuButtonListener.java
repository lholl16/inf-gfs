package net.lholl.school.connectfour.gui.listener;

import net.lholl.school.connectfour.ConnectFour;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BackToMainMenuButtonListener implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent e) {
		ConnectFour.instance().guiManager().showMainMenu();
	}
}
