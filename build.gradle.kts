import com.github.jengelman.gradle.plugins.shadow.transformers.Log4j2PluginsCacheFileTransformer
plugins {
    id("java-library")
    id("maven-publish")
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

group = "net.lholl.school"
version = "1.3-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    api("com.jgoodies:jgoodies-forms:1.9.0")
    implementation("org.jetbrains:annotations:23.0.0")
    implementation("org.jetbrains:annotations:23.0.0")
}

java {
    withSourcesJar()
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-Xlint:deprecation")
    options.encoding = Charsets.UTF_8.name()
}

publishing {
    publications.create<MavenPublication>("maven") {
        artifactId = project.name.toLowerCase()
        from(components["java"])
    }

    repositories.maven("https://maven.pkg.github.com/lholl16/ConnectFour-GFS") {
        name = "github"
        authentication { create<BasicAuthentication>("header") }
        credentials(PasswordCredentials::class)
    }
}

tasks {
    jar {
        manifest.attributes(
            "Implementation-Vendor" to "lholl",
            "Implementation-Version" to project.version,
            "Implementation-Title" to project.name,
            "Main-Class" to "net.lholl.school.connectfour.ConnectFour",
            "Multi-Release" to "true",
        )
    }

    shadowJar {
        transform(Log4j2PluginsCacheFileTransformer::class.java)
    }

    build {
        dependsOn(shadowJar)
    }
}