package net.lholl.school.connectfour.game;

import net.lholl.school.connectfour.ConnectFour;
import net.lholl.school.connectfour.game.comp.ComputerOpponent;

public class MultiPlayerGame extends Game {

	public MultiPlayerGame(Player firstPlayer, Player secondPlayer) {
		super(firstPlayer, secondPlayer);
	}

	@Override
	public void start() {
		if (this.gameRunning) return;
		this.gameRunning = true;
		this.currentPlayer = this.firstPlayer;
		ConnectFour.instance().gameManager().info().setText("Spieler 1 ist dran! Wähle eine Spalte aus.");
	}

	@Override
	public void stop() {
		if (!this.gameRunning) return;
		this.gameRunning = false;
		this.currentPlayer = null;
	}

	@Override
	public void checkForWin() {
		this.gameBoard.checkForWin();
	}

	@Override
	public void doMove(int columnIndex) {
		for (int i = 5; i >= 0; i--) {
			if (this.gameBoard.getBoardValue(i, columnIndex) == 0) {
				this.gameBoard.setBoardValue(i, columnIndex, this.currentPlayer().id());
//				if (currentPlayer == OpponentType.PLAYER_1) board[i][columnIndex] = 2;
//				else board[i][columnIndex] = 1;
				break;
			}
		}
	}

	@Override
	public void submitColumnSelect(int column) {
		if (!this.gameRunning) return;
		if (!this.gameBoard.validateSelection(column)) ConnectFour.instance().gameManager().info().setText("Die Spalte ist bereits voll! Bitte wähle eine andere Spalte aus.");
		else {
			doMove(column);
			ConnectFour.instance().guiManager().updateGameDisplay();
			int result = this.gameBoard.checkForWin();
			if (result != -1) {
				if (result == 2) ConnectFour.instance().gameManager().info().setText("Spieler 2 hat gewonnen!");
				else if (result == 1) ConnectFour.instance().gameManager().info().setText("Spieler 1 hat gewonnen!");
				else if (result == 0) ConnectFour.instance().gameManager().info().setText("Unentschieden!");
				this.stop();
			}
			this.togglePlayer();
		}
	}

	private void togglePlayer() {
		if (this.currentPlayer == null) return;
		if (this.currentPlayer.id() == this.firstPlayer.id()) this.currentPlayer = this.secondPlayer;
		else this.currentPlayer = this.firstPlayer;
		ConnectFour.instance().gameManager().info().setText("Spieler " + this.currentPlayer.id() + " ist dran! Wähle eine Spalte aus.");
	}
}
