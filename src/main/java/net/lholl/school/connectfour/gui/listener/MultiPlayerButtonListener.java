package net.lholl.school.connectfour.gui.listener;

import net.lholl.school.connectfour.ConnectFour;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MultiPlayerButtonListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		ConnectFour.instance().guiManager().showGameDisplay();
		ConnectFour.instance().guiManager().gameDisplay().header.setText("Mehrspieler");
		ConnectFour.instance().gameManager().createNewMultiPlayerGame();
		ConnectFour.instance().gameManager().startGame();
	}
}
