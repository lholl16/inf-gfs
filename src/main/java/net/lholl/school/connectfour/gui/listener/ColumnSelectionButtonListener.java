package net.lholl.school.connectfour.gui.listener;

import net.lholl.school.connectfour.ConnectFour;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ColumnSelectionButtonListener implements ActionListener {

	private final int columnIndex;

	public ColumnSelectionButtonListener(int columnIndex) {
		this.columnIndex = columnIndex;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ConnectFour.instance().gameManager().submitColumnSelect(columnIndex);
	}
}
