package net.lholl.school.connectfour.game;

import net.lholl.school.connectfour.ConnectFour;
import net.lholl.school.connectfour.game.comp.ComputerDifficulty;
import net.lholl.school.connectfour.game.comp.ComputerOpponent;

public class SinglePlayerGame extends Game {

	private final ComputerOpponent computerOpponent;

	public SinglePlayerGame(ComputerDifficulty difficulty, Player firstPlayer) {
		super(firstPlayer, null);
		this.computerOpponent = new ComputerOpponent(this, "Computer", difficulty);
	}

	@Override
	public void start() {
		if (this.gameRunning) return;
		this.gameRunning = true;
		this.currentPlayer = this.firstPlayer;
		ConnectFour.instance().gameManager().info().setText("Du bist dran! Wähle eine Spalte aus.");
	}

	@Override
	public void stop() {
		if (!this.gameRunning) return;
		this.gameRunning = false;
		this.currentPlayer = null;
	}

	@Override
	public void checkForWin() {
		this.gameBoard.checkForWin();
	}

	@Override
	public void doMove(int columnIndex) {
		for (int i = 5; i >= 0; i--) {
			if (this.gameBoard.getBoardValue(i, columnIndex) == 0) {
				this.gameBoard.setBoardValue(i, columnIndex, this.currentPlayer().id());
//				if (currentPlayer == OpponentType.PLAYER_1) board[i][columnIndex] = 2;
//				else board[i][columnIndex] = 1;
				break;
			}
		}
	}

	@Override
	public void submitColumnSelect(int column) {
		this.submitColumnSelect(column, false);
	}

	public void submitColumnSelect(int column, boolean forceComp) {
		if (!this.gameRunning) return;
		if (this.currentPlayer.id() == this.computerOpponent.id() && !forceComp) return;
		if (!this.gameBoard.validateSelection(column)) ConnectFour.instance().gameManager().info().setText("Die Spalte ist bereits voll! Bitte wähle eine andere Spalte aus.");
		else {
			doMove(column);
			ConnectFour.instance().guiManager().updateGameDisplay();
			int result = this.gameBoard.checkForWin();
			if (result != -1) {
				if (result == 2) ConnectFour.instance().gameManager().info().setText("Du hast gewonnen!");
				else if (result == 1) ConnectFour.instance().gameManager().info().setText("Dein Gegner hat gewonnen");
				else if (result == 0) ConnectFour.instance().gameManager().info().setText("Unentschieden!");
				this.stop();
			}
			this.togglePlayer();
		}
	}

	private void togglePlayer() {
		if (this.currentPlayer == null) return;
		if (this.currentPlayer.id() == this.firstPlayer.id()) {
			this.currentPlayer = this.computerOpponent;
			ConnectFour.instance().gameManager().info().setText("Dein Gegner ist dran!");
			this.computerOpponent.updateBoard();
			new Thread(this.computerOpponent::doMove).start();
		} else {
			this.currentPlayer = this.firstPlayer();
			ConnectFour.instance().gameManager().info().setText("Du bist dran! Wähle eine Spalte aus.");
		}
	}

	@Override
	public Player secondPlayer() {
		return this.computerOpponent();
	}

	public ComputerOpponent computerOpponent() {
		return computerOpponent;
	}
}
