package net.lholl.school.connectfour.game;

public class Player {

	private final int id;
	private final String name;

	public Player(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public String name() {
		return name;
	}

	public int id() {
		return id;
	}
}
