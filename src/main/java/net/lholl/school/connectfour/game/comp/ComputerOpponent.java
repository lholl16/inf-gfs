package net.lholl.school.connectfour.game.comp;

import net.lholl.school.connectfour.ConnectFour;
import net.lholl.school.connectfour.game.GameBoard;
import net.lholl.school.connectfour.game.Player;
import net.lholl.school.connectfour.game.SinglePlayerGame;
import org.jetbrains.annotations.NotNull;

public class ComputerOpponent extends Player {

	private final SinglePlayerGame singlePlayerGame;
	private final ComputerDifficulty difficulty;

	private final double[][] rated = {
			{0.03, 0.04, 0.05, 0.07, 0.05, 0.04, 0.03},
			{0.04, 0.06, 0.08, 0.10, 0.08, 0.06, 0.04},
			{0.05, 0.08, 0.11, 0.13, 0.11, 0.08, 0.05},
			{0.05, 0.08, 0.11, 0.13, 0.11, 0.08, 0.05},
			{0.04, 0.06, 0.08, 0.10, 0.08, 0.06, 0.04},
			{0.03, 0.04, 0.05, 0.07, 0.05, 0.04, 0.03}};

	private GameBoard boardCopy;

	private int minY;

	public ComputerOpponent(@NotNull SinglePlayerGame singlePlayerGame, String name, ComputerDifficulty difficulty) {
		super(1, name);
		this.singlePlayerGame = singlePlayerGame;
		this.difficulty = difficulty;
		this.boardCopy = new GameBoard(singlePlayerGame.gameBoard().board());
		this.minY = 0;
	}

	public void updateBoard() {
		this.boardCopy = new GameBoard(singlePlayerGame.gameBoard().board());
	}

	public void doMove() {
		if (this.boardCopy.checkForWin() != -1) return;
		minY = locateMinY();
		ConnectFour.instance().gameManager().submitColumnSelect(minY, true);
	}

	public int locateMinY() {
		if (this.boardCopy.checkForWin() != -1) return this.boardCopy.checkForWin();
		var minValue = 9999D;
		var maxValue = -9999D;
		for (int i = 0; i < 7; i++) {
			if (this.boardCopy.getBoardValue(0, i) == 0) {
				doMove(i, 1);
				if (this.boardCopy.checkForWin() == 1) {
					undoMove(i);
					return i;
				}
				var value = max(difficulty.maxCalculationDepth, maxValue, minValue);
				if (value < minValue) {
					minValue = value;
					minY = i;
				}
				undoMove(i);
			}
		}
		return minY;
	}

	private double max(int nextCalculations, double maxValue, double minValue) {
		if (this.boardCopy.checkForWin() != -1) {
			if (this.boardCopy.checkForWin() == 1) return 0;
			return this.boardCopy.checkForWin();
		}
		if (nextCalculations == 0) return getRating();
//        maxValue = -9999;
		for (int i = 0; i < 7; i++) {
			if (this.boardCopy.getBoardValue(0, i) == 0) {
				doMove(i, 2);
				var value = min(nextCalculations - 1, maxValue, minValue);
				if (value > maxValue) maxValue = value;
				undoMove(i);
			}
		}
		return maxValue;
	}

	private double min(int nextCalculations, double maxValue, double minValue) {
		if (this.boardCopy.checkForWin() != -1) {
			if (this.boardCopy.checkForWin() == 1) return 0;
			return this.boardCopy.checkForWin();
		}
		if (nextCalculations == 0) return getRating();
		for (int i = 0; i < 7; i++) {
			if (this.boardCopy.getBoardValue(0, i) == 0) {
				doMove(i, 1);
				var value = max(nextCalculations - 1, maxValue, minValue);
				if (value < minValue) minValue = value;
				undoMove(i);
			}
		}
		return minValue;
	}

	private double getRating() {
		var rateing = 1.0D;
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 7; j++) {
				if (this.boardCopy.getBoardValue(i, j) == 2) {
					rateing = rateing + rated[i][j];
				}
				if (this.boardCopy.getBoardValue(i, j) == 1) {
					rateing = rateing - rated[i][j];
				}
			}
		}
		return rateing;
	}

	private void doMove(int column, int player){
		for (int i = 5; i >= 0; i--)
			if (this.boardCopy.getBoardValue(i, column) == 0) {
				this.boardCopy.setBoardValue(i, column, player);
				break;
			}
	}

	public void undoMove(int column) {
		for (int i = 0; i < 6; i++)
			if (this.boardCopy.getBoardValue(i, column) != 0) {
				this.boardCopy.setBoardValue(i, column, 0);
				break;
			}
	}
}
