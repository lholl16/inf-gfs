package net.lholl.school.connectfour.game.comp;

public enum ComputerDifficulty {
	EASY(2),
	MEDIUM(3),
	HARD(7);

	public final int maxCalculationDepth;

	ComputerDifficulty(int maxCalculationDepth) {
		this.maxCalculationDepth = maxCalculationDepth;
	}
}
