package net.lholl.school.connectfour;

import net.lholl.school.connectfour.game.GameManager;
import net.lholl.school.connectfour.gui.GUIManager;

public class ConnectFour {

	private static ConnectFour instance;

	public static void main(String[] args) {
		instance = new ConnectFour();
		instance.init();
	}

	private final GUIManager guiManager;
	private final GameManager gameManager;

	public ConnectFour() {
		this.guiManager = new GUIManager();
		this.gameManager = new GameManager();
	}

	private void init() {
		guiManager.init();
		guiManager.showMainMenu();
	}

	public static ConnectFour instance() {
		return instance;
	}

	public GUIManager guiManager() {
		return guiManager;
	}

	public GameManager gameManager() {
		return gameManager;
	}
}
