package net.lholl.school.connectfour.gui;

import net.lholl.school.connectfour.gui.pages.MainMenu;
import net.lholl.school.connectfour.gui.pages.GameDisplay;
import net.lholl.school.connectfour.gui.pages.SinglePlayerSelection;

import javax.swing.*;

public class GUIManager extends JFrame {

	private SinglePlayerSelection singlePlayerSelection;
	private GameDisplay gameDisplay;

	public GUIManager() {
		super("Connect Four");
	}

	public void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 600);
		setResizable(false);
		setVisible(true);
	}

	public void showMainMenu() {
		removeContentPane();
		getContentPane().add(new MainMenu().mainMenu);
		refresh();
		pack();
	}

	public void showSinglePlayerMenu() {
		removeContentPane();
		singlePlayerSelection = new SinglePlayerSelection();
		getContentPane().add(singlePlayerSelection.singlePlayerSelection);
		refresh();
		pack();
	}

	public void showGameDisplay() {
		removeContentPane();
		gameDisplay = new GameDisplay();
		gameDisplay.drawField();
		getContentPane().add(gameDisplay.gameDisplay);
		refresh();
		pack();
	}

	public SinglePlayerSelection singlePlayerSelection() {
		return singlePlayerSelection;
	}

	public void updateGameDisplay() {
		if (this.gameDisplay != null) this.gameDisplay.update();
	}

	public GameDisplay gameDisplay() {
		return gameDisplay;
	}

	private void removeContentPane() {
		getContentPane().removeAll();
	}

	private void refresh() {
		revalidate();
		repaint();
	}


}
