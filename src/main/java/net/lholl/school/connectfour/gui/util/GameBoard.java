package net.lholl.school.connectfour.gui.util;

import javax.swing.*;
import java.awt.*;

public class GameBoard extends JPanel {

	private int[][] board;

	public GameBoard() {
	}

	public GameBoard(int[][] board) {
		this.board = board;
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 6; j++) {
				g.setColor(Color.white);
				if (board != null && board[j][i] == 1) g.setColor(Color.red);
				else if (board != null && board[j][i] == 2) g.setColor(Color.yellow);
				g.fillOval(i * 75 + 10 * (i + 1), j * 75 + 10 * (j + 1) + 50, 75, 75);
			}
		}
	}
}
