package net.lholl.school.connectfour.game;

import net.lholl.school.connectfour.ConnectFour;
import net.lholl.school.connectfour.game.comp.ComputerDifficulty;

import javax.swing.*;

public class GameManager {

	private Game currentGame;
	private JLabel info;

	public GameManager() {
		this.currentGame = null;
		this.info = null;
	}

	public void createNewSinglePlayerGame(ComputerDifficulty difficulty) {
		if (this.currentGame != null) return;
		this.currentGame = new SinglePlayerGame(difficulty, new Player(2, "Spieler"));
	}

	public void createNewMultiPlayerGame() {
		if (this.currentGame != null) return;
		this.currentGame = new MultiPlayerGame(new Player(1, "Spieler 1"), new Player(2, "Spieler 2"));
	}

	public void startGame() {
		if (this.currentGame == null) return;
		this.info = ConnectFour.instance().guiManager().gameDisplay().info;
		this.currentGame.start();
	}

	public void submitColumnSelect(int columnIndex) {
		this.currentGame.submitColumnSelect(columnIndex);
	}

	public void submitColumnSelect(int columnIndex, boolean forceComp) {
		if (this.currentGame instanceof SinglePlayerGame) {
			var game = (SinglePlayerGame) this.currentGame;
			game.submitColumnSelect(columnIndex, forceComp);
		}
	}

	public JLabel info() {
		return info;
	}

	public Game currentGame() {
		return currentGame;
	}
}
