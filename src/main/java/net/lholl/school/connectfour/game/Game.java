package net.lholl.school.connectfour.game;

public abstract class Game {

	protected GameBoard gameBoard;
	protected final Player firstPlayer, secondPlayer;
	protected Player currentPlayer;
	protected boolean gameRunning;

	public Game(Player firstPlayer, Player secondPlayer) {
		this.gameBoard = new GameBoard();
		this.firstPlayer = firstPlayer;
		this.secondPlayer = secondPlayer;
		this.currentPlayer = null;
		this.gameRunning = false;
	}

	abstract public void start();
	abstract public void stop();
	abstract public void checkForWin();
	abstract public void doMove(int columnIndex);
	abstract public void submitColumnSelect(int column);
	public GameBoard gameBoard() {
		return gameBoard;
	}

	public Player firstPlayer() {
		return firstPlayer;
	}

	public Player secondPlayer() {
		return secondPlayer;
	}

	public Player currentPlayer() {
		return currentPlayer;
	}

	public boolean gameRunning() {
		return gameRunning;
	}
}
