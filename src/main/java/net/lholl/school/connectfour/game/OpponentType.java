package net.lholl.school.connectfour.game;

public enum OpponentType {

	PLAYER_1,
	PLAYER_2,
	COMPUTER,

}
