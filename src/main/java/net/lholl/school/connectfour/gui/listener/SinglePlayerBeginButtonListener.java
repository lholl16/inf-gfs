package net.lholl.school.connectfour.gui.listener;

import net.lholl.school.connectfour.ConnectFour;
import net.lholl.school.connectfour.game.comp.ComputerDifficulty;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SinglePlayerBeginButtonListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		ConnectFour.instance().guiManager().showGameDisplay();
		var difficulty = ComputerDifficulty.values()[ConnectFour.instance().guiManager().singlePlayerSelection().selection.getSelectedIndex()];
		ConnectFour.instance().gameManager().createNewSinglePlayerGame(difficulty);
		ConnectFour.instance().gameManager().startGame();
	}
}
